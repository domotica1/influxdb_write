# python3 -m venv env
# source env/bin/activate
# pip3 install -r requeriments.txt
# deactivate

import sys
from datetime import datetime
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS


def write_influx_insta(ig_account, followers):
    # You can generate an API token from the "API Tokens Tab" in the UI
    token = "psLoWtJIhbuy_PAuLPMbt0_yK4168oBo8GGCwXLKHIUxEZAocMJg9hwOoCZCQ7QLCFXk5M8ehn6wgE56UqwOsw=="
    org = "mgu"
    bucket = "instagram"

    with InfluxDBClient(url="http://localhost:8086", token=token, org=org) as client:
        write_api = client.write_api(write_options=SYNCHRONOUS)

        point = Point(ig_account) \
            .tag("account", ig_account) \
            .field("followers", float(followers)) \
            .time(datetime.utcnow(), WritePrecision.NS)

        write_api.write(bucket, org, point)
    client.close()
